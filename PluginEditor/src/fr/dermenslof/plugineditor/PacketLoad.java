package fr.dermenslof.plugineditor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class PacketLoad extends Packet
{
	public String	name;
	public String	jarName;

	public PacketLoad()
	{
		super();
	}

	public PacketLoad(String name, String jarName)
	{
		super();
		this.name = name;
		this.jarName = jarName;
		setSize();
	}

	public void read(DataInputStream dis) throws IOException
	{
		this.name = dis.readUTF();
		this.jarName = dis.readUTF();
		loadPlugin();
	}

	public void write(DataOutputStream dos) throws IOException
	{
		dos.writeUTF(this.name);
		dos.writeUTF(this.jarName);
	}

	private void setSize()
	{
		size += this.name.length() + this.jarName.length();
	}

	private void loadPlugin()
	{
		PluginControl pc = new PluginControl();
		File org = new File("plugins", this.name + ".jar");
		File src = new File("beta_plugins", this.jarName);
		File dst = new File("plugins", this.name + ".jar");
		File save = null;
		if (org.exists())
		{
			Plugin pl = Bukkit.getServer().getPluginManager().getPlugin(this.name);
			pc.unloadPlugin(pl, true);
			save = new File("plugins/save_plugins", this.name);
			new File("plugins/save_plugins").mkdirs();
			move(org, save, true);
		}
		new File("plugins").mkdirs();
		if (src.exists())
		{
				move(src, dst, false);
				Plugin pl = pc.loadPlugin(dst);
				if (pl != null)
					pc.enablePlugin(pl);
		}
	}

	private void move(File src, File dst, boolean deleteSrc)
	{
		InputStream inStream = null;
		OutputStream outStream = null;
		try
		{

			inStream = new FileInputStream(src);
			outStream = new FileOutputStream(dst);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inStream.read(buffer)) > 0)
				outStream.write(buffer, 0, length);
			inStream.close();
			outStream.close();
			if (deleteSrc)
				src.delete();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
