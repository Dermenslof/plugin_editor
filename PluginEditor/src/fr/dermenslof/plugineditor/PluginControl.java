package fr.dermenslof.plugineditor;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

public class PluginControl
{
	public PluginDescriptionFile getDescription(File file)
	{
		try
		{
			JarFile jar = new JarFile(file);
			ZipEntry zip = jar.getEntry("plugin.yml");
			if (zip == null)
			{
				jar.close();
				return null;
			}
			PluginDescriptionFile pdf = new PluginDescriptionFile(jar.getInputStream(zip));
			jar.close();
			return pdf;
		}
		catch (InvalidDescriptionException | IOException ioe)
		{
			ioe.printStackTrace();
		}
		return null;
	}

	public File getFile(JavaPlugin plugin)
	{
		try
		{
			Field file = JavaPlugin.class.getDeclaredField("file");
			file.setAccessible(true);
			return (File) file.get(plugin);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void enablePlugin(Plugin plugin)
	{
		Bukkit.getPluginManager().enablePlugin(plugin);
	}

	public void disablePlugin(Plugin plugin)
	{
		Bukkit.getPluginManager().disablePlugin(plugin);
	}

	public Plugin loadPlugin(File plugin)
	{
		try
		{
			Plugin p = Bukkit.getPluginManager().loadPlugin(plugin);
			try
			{
				p.onLoad();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return p;
		}
		catch (InvalidPluginException | InvalidDescriptionException | UnknownDependencyException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public boolean unloadPlugin(Plugin plugin, Boolean ReloadDependents)
	{
		PluginManager pluginManager = Bukkit.getPluginManager();

		String pName = plugin.getName();
		List<Plugin> plugins = null;
		Map<String, Plugin> names = null;
		Map<String, Command> commands = null;
		ArrayList<Plugin> reload = new ArrayList<Plugin>();
		if (ReloadDependents.booleanValue())
		{
			for (Plugin p : pluginManager.getPlugins())
			{
				List<String> depend = p.getDescription().getDepend();
				if (depend != null)
				{
					for (String s : depend)
					{
						if (s.equals(pName))
						{
							if (!reload.contains(p))
							{
								reload.add(p);
								unloadPlugin(p, Boolean.valueOf(false));
							}
						}
					}
				}
				List<String> softDepend = p.getDescription().getSoftDepend();
				if (softDepend != null)
				{
					for (String s : softDepend)
					{
						if (s.equals(pName))
						{
							if (!reload.contains(p))
							{
								reload.add(p);
								unloadPlugin(p, Boolean.valueOf(false));
							}
						}
					}
				}
			}
		}
		SimpleCommandMap commandMap;
		try
		{
			Field pluginsField = pluginManager.getClass().getDeclaredField("plugins");
			Field lookupNamesField = pluginManager.getClass().getDeclaredField("lookupNames");
			Field commandMapField = pluginManager.getClass().getDeclaredField("commandMap");
			Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");

			pluginsField.setAccessible(true);
			lookupNamesField.setAccessible(true);
			commandMapField.setAccessible(true);
			knownCommandsField.setAccessible(true);

			plugins = (List<Plugin>) pluginsField.get(pluginManager);
			names = (Map<String, Plugin>) lookupNamesField.get(pluginManager);
			commandMap = (SimpleCommandMap) commandMapField.get(pluginManager);
			commands = (Map<String, Command>) knownCommandsField.get(commandMap);
		}
		catch (NoSuchFieldException | IllegalAccessException e)
		{
			e.printStackTrace();
			return false;
		}
		Bukkit.getScheduler().cancelTasks(plugin);
		if (commandMap != null)
		{
			synchronized (commandMap)
			{
				Iterator<Map.Entry<String, Command>> it = commands.entrySet().iterator();
				while (it.hasNext())
				{
					Map.Entry<String, Command> entry = it.next();
					if ((entry.getValue() instanceof PluginCommand))
					{
						PluginCommand c = (PluginCommand) entry.getValue();
						if (c.getPlugin() == plugin)
						{
							c.unregister(commandMap);
							it.remove();
						}
					}
				}
			}
		}
		disablePlugin(plugin);
		synchronized (pluginManager)
		{
			if ((plugins != null) && (plugins.contains(plugin)))
			{
				plugins.remove(plugin);
			}
			if ((names != null) && (names.containsKey(pName)))
			{
				names.remove(pName);
			}
		}
		JavaPluginLoader jpl = (JavaPluginLoader) plugin.getPluginLoader();
		Field loaders = null;
		try
		{
			loaders = jpl.getClass().getDeclaredField("loaders");
			loaders.setAccessible(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			Map<String, ?> loaderMap = (Map<String, ?>) loaders.get(jpl);
			loaderMap.remove(plugin.getDescription().getName());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		ClassLoader cl = plugin.getClass().getClassLoader();
		try
		{
			((URLClassLoader) cl).close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		System.gc();
		if (ReloadDependents.booleanValue())
		{
			for (int i = 0; i < reload.size(); i++)
			{
				enablePlugin(loadPlugin(getFile((JavaPlugin) reload.get(i))));
			}
		}
		return true;
	}
}