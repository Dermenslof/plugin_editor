package fr.dermenslof.plugineditor;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class PluginEditor extends JavaPlugin
{
	public static PluginEditor	instance;
	public SimpleFileServer		server;
	public BukkitTask			task;

	@Override
	public void onEnable()
	{
		instance = this;
		this.server = new SimpleFileServer();
		this.task = getServer().getScheduler().runTaskAsynchronously(this, this.server);
	}

	@Override
	public void onDisable()
	{
		this.server.stop();
		this.task.cancel();
	}
}