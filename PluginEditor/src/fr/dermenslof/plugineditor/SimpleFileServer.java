package fr.dermenslof.plugineditor;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleFileServer implements Runnable
{
	public final static int	SOCKET_PORT	= 25665;
	private boolean			doStart;
	private ServerSocket	servsock;

	@Override
	public void run()
	{
		try
		{
			start();
		}
		catch (IOException e)
		{
		}
	}

	public void stop()
	{
		this.doStart = false;
		if (this.servsock != null)
			try
			{
				this.servsock.close();
			}
			catch (IOException e)
			{
			}
	}

	public void start() throws IOException
	{
		this.doStart = true;
		servsock = null;
		Socket sock = null;
		try
		{
			servsock = new ServerSocket(SOCKET_PORT);
			while (this.doStart)
			{
				try
				{
					sock = servsock.accept();
					System.out.println("Accepted connection : " + sock);
					DataInputStream dis = new DataInputStream(sock.getInputStream());
					Packet packet = new Packet().handle(dis);
					if (packet != null)
						packet.read(dis);
					System.out.println("Done.");
				}
				finally
				{
					if (sock != null)
						sock.close();
				}
			}
		}
		finally
		{
			if (servsock != null)
				servsock.close();
		}
	}
}