package fr.dermenslof.plugineditor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Packet
{
	@SuppressWarnings("unused")
	private static Packet				instance	= init();
	private static Map<Byte, Class<?>>	PACKET;

	protected int						size;
	private byte						id;

	private static Packet init()
	{
		PACKET = new HashMap<Byte, Class<?>>();
		PACKET.put((byte) 1, PacketFile.class);
		PACKET.put((byte) 2, PacketLoad.class);
		return new Packet();
	}

	public Packet()
	{
		this.id = -1;
		this.size = 5;
	}

	public void read(DataInputStream dis) throws IOException
	{
		;
	}

	public void write(DataOutputStream dos) throws IOException
	{
		;
	}

	@SuppressWarnings("unchecked")
	public <T extends Packet> T handle(DataInputStream dis) throws IOException
	{
		this.id = dis.readByte();
		Class<?> clazz = PACKET.get(this.id);
		if (clazz == null)
			throw new IOException("bad PACKET_ID");
		this.size = dis.readInt();
		try
		{
			return (T)clazz.cast(clazz.newInstance());
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void handle(DataOutputStream dos) throws IOException
	{
		Class<?> clazz = PACKET.get(getId());
		if (clazz == null)
			throw new IOException("bad PACKET_ID");
		dos.writeByte(getId());
		dos.writeInt(this.size);
	}

	public int getSize()
	{
		return this.size;
	}

	public byte getId()
	{
		if (this.id < 0)
		{
			this.id = 0;
			for (Entry<Byte, Class<?>> e : PACKET.entrySet())
			{
				try
				{
					if (Class.forName(e.getValue().getName()).isInstance(this))
					{
						this.id = e.getKey();
						break;
					}
				}
				catch (Exception ex) {}
			}
		}
		return this.id;
	}
}