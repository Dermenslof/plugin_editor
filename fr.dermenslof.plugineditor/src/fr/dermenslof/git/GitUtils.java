package fr.dermenslof.git;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.swt.widgets.Display;

import fr.dermenslof.plugineditor.ui.PluginEditor;
import fr.dermenslof.plugineditor.ui.Privacy;
import fr.dermenslof.plugineditor.ui.TreeObjectView;
import fr.dermenslof.project.ProjectUtil;

public class GitUtils
{
	public static File getWorkspacePath()
	{
		return PluginEditor.getWorkspace().getRoot().getLocation().toFile();
	}

	public static RepositoryService getService()
	{
		RepositoryService service = new RepositoryService();
		// service.getClient().setOAuth2Token(Privacy.token);
		service.getClient().setCredentials(Privacy.user, Privacy.password);
		return service;
	}

	public static List<Repository> explore()
	{
		List<Repository> list = new ArrayList<Repository>();
		try
		{
			return getService().getRepositories(Privacy.user.toLowerCase());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return list;
	}

	public static String connect()
	{
		String msg = null;
		try
		{
			getService().getRepositories(Privacy.user.toLowerCase());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			msg = e.getMessage().contains("401") ? "invalid username or password" : e.getMessage();
		}
		return msg;
	}

	public static Git clone(String url, String repoName)
	{
		Git repo = null;
		try
		{
			CloneCommand cmd = Git.cloneRepository();
			File dir = new File(getWorkspacePath(), "temp_" + repoName);
			cmd.setDirectory(dir);
			cmd.setCredentialsProvider(new UsernamePasswordCredentialsProvider(Privacy.user, Privacy.password));
			cmd.setURI(url);
			repo = cmd.call();
			IProject project = ProjectUtil.createProject(repoName);
			for (File fi : dir.listFiles())
				Files.move(fi.toPath(), new File(new File(getWorkspacePath(), repoName), fi.getName()).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			dir.delete();
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}
		catch (IOException | GitAPIException | CoreException e)
		{
			TreeObjectView.showMessage(e.getMessage());
			// e.printStackTrace();
		}
		return repo;
	}

	public static org.eclipse.jgit.lib.Repository getLocalGit(File path)
	{
		FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
		repositoryBuilder.setMustExist(true);
		repositoryBuilder.setGitDir(new File(path, ".git"));
		try
		{
			org.eclipse.jgit.lib.Repository repo = repositoryBuilder.build();
			if (repo.getRef("HEAD") != null)
				return repo;
		}
		catch (IOException e)
		{
			// e.printStackTrace();
		}
		return null;
	}

	public static Repository createRemoteRepo(String name)
	{
		Repository repo = new Repository();

		repo.setName(name);
		try
		{
			return getService().createRepository(repo);
		}
		catch (IOException e)
		{
			if (e.getMessage().contains("422"))
				TreeObjectView.showMessage("name already exists on this account");
			else
				TreeObjectView.showMessage(e.getMessage());
			// e.printStackTrace();
		}
		return null;
	}

	public static Git init(String name, String url)
	{
		Git git = null;

		InitCommand cmd = new InitCommand();
		cmd.setDirectory(new File(getWorkspacePath(), name));
		try
		{
			git = cmd.call();
			StoredConfig config = git.getRepository().getConfig();
			config.setString("remote", "origin", "url", url);
			config.save();
		}
		catch (GitAPIException | IOException e)
		{
			TreeObjectView.showMessage(e.getMessage());
			// e.printStackTrace();
		}
		return git;
	}

	public static void pull(String name)
	{
		try
		{
			Git git = Git.open(new File(getWorkspacePath(), name));
			if (git != null)
			{
				PullCommand pcmd = git.pull();
				PullResult res = pcmd.call();
				String[] tab = res.toString().split("\n");
				String m = Arrays.asList(Arrays.copyOfRange(tab, 1, tab.length - 1)).toString()
						.replaceAll("[\\[\\],]", "");
				if (!m.isEmpty())
					TreeObjectView.showMessage(m);
			}
		}
		catch (IOException | GitAPIException ex)
		{
			TreeObjectView.showMessage(ex.getMessage());
			// ex.printStackTrace();
		}
	}

	public static boolean status(String name)
	{
		try
		{
			Git git = Git.open(new File(getWorkspacePath(), name));
			git.add().addFilepattern(".").call();
			Status status = git.status().call();
			return status.hasUncommittedChanges();
		}
		catch (IOException | NoWorkTreeException | GitAPIException e)
		{
			// e.printStackTrace();
		}
		return false;
	}

	public static void pushAll(String name)
	{
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(name);
		try
		{
			IMarker[] errors =  project.findMarkers(IMarker.PROBLEM, true, IResource.DEPTH_INFINITE);
			if (errors.length > 0)
			{
				StringBuilder sb = new StringBuilder();
				for (IMarker m : errors)
				{
					Iterator<Entry<String, Object>> it = m.getAttributes().entrySet().iterator();
					while (it.hasNext())
					{
						Entry<String, Object> e = it.next();
						if (!e.getValue().toString().trim().isEmpty())
							sb.append("\n" + e.getKey() + ": " + e.getValue());
					}
				}
				TreeObjectView.showMessage("Error: Project \"" + name + "\" contains errors" + sb.toString());
				return;
			}
		}
		catch (CoreException e)
		{
			TreeObjectView.showMessage("Error: Unknow error");
			e.printStackTrace();
			return;
		}
		try
		{
			Git git = Git.open(new File(getWorkspacePath(), name));
			if (git != null)
			{
				git.add().addFilepattern(".").call();
				String msg = "";
				InputDialog dlg = new InputDialog(Display.getCurrent().getActiveShell(), "", "commit: ", "", null);
				if (dlg.open() == Window.OK)
					msg = dlg.getValue();
				else
					return;
				RevCommit commit = git.commit().setAll(true).setMessage(msg).call();
				PushCommand cmd = git.push();
				cmd.setCredentialsProvider(new UsernamePasswordCredentialsProvider(Privacy.user, Privacy.password));
				StringBuilder sb = new StringBuilder();
				Iterator<PushResult> it = cmd.setPushAll().call().iterator();
				while (it.hasNext())
				{
					PushResult pr = it.next();
					if (!pr.getMessages().trim().isEmpty())
						sb.append(pr.getMessages());
				}
				if (sb.length() > 0)
					TreeObjectView.showMessage(sb.toString());
				else
				{
					TreeObjectView.showMessage("repository:\t"
							+ git.getRepository().getConfig().getString("remote", "origin", "url") + "\ncommit:\t\t"
							+ commit.name() + "\nat:\t\t\t" + SimpleDateFormat.getInstance().format(new Date())
							+ "\nmessage:\t\"" + commit.getFullMessage() + "\"");
				}
			}
		}
		catch (IOException | GitAPIException ex)
		{
			TreeObjectView.showMessage(ex.getMessage());
			// ex.printStackTrace();
		}
	}

//	public static String checkCompilerCompliance(IProject project)
//	{
//			try
//			{
//				project.build(IncrementalProjectBuilder.FULL_BUILD, new NullProgressMonitor());
//			}
//			catch (CoreException e)
//			{
//				return "Project compiler settings changed. Clean your project.";
//			}
//			return "";
//	}
}