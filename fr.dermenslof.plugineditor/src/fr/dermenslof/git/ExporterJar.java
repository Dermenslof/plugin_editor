package fr.dermenslof.git;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

public class ExporterJar implements Runnable
{
	private static byte[]	BUFFER	= new byte[1024];
	private String			projectName;

	public ExporterJar(String name)
	{
		this.projectName = name;
	}

	public void run()
	{
		File base = GitUtils.getWorkspacePath();
		SimpleDateFormat sdf = new SimpleDateFormat("dd\\MM_HH:mm");
		String target = projectName + "_SNAPSHOT_" + sdf.format(new Date()) + ".jar";
		Manifest manifest = new Manifest();
		manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		JarOutputStream jos;
		try
		{
			File output = new File(base, "builds_plugin" + "/" + projectName);
			output.mkdirs();
			output = new File(output, target);
			jos = new JarOutputStream(new FileOutputStream(output), manifest);
			jos.setLevel(9);
			for (File f : new File(base, projectName + "/bin").listFiles())
				add(f, jos, new File(base, projectName + "/bin").toString());
			jos.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addFile(File folder, List<File> files)
	{
		for (File f : folder.listFiles())
		{
			if (f.isDirectory())
				addFile(f, files);
			else
				files.add(f);
		}
		files.add(folder);
	}

	private void add(File source, JarOutputStream target, String path) throws IOException
	{
		String name = source.getPath().replace("\\", "/");
		name = name.substring(path.length() + 1, name.length());
		BufferedInputStream bis = null;
		try
		{
			if (source.isDirectory())
			{
				File[] subFiles = source.listFiles();
				if (subFiles.length < 1)
					return;
				if (!name.isEmpty())
				{
					if (!name.endsWith("/"))
						name += "/";
					System.out.println(name);
					JarEntry entry = new JarEntry(name);
					entry.setTime(source.lastModified());
					target.putNextEntry(entry);
					target.closeEntry();
				}
				for (File nestedFile : subFiles)
					add(nestedFile, target, path);
				return;
			}
			System.out.println(name);
			JarEntry entry = new JarEntry(name);
			entry.setTime(source.lastModified());
			target.putNextEntry(entry);
			bis = new BufferedInputStream(new FileInputStream(source));
			int count;
			while ((count = bis.read(BUFFER)) != -1)
				target.write(BUFFER, 0, count);
			target.closeEntry();
		}
		finally
		{
			if (bis != null)
				bis.close();
		}
	}
}