package fr.dermenslof.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PacketFile extends Packet
{
	public File	file;

	public PacketFile()
	{
		super();
	}

	public PacketFile(File file)
	{
		super();
		this.file = file;
		setSize();
	}

	public void read(DataInputStream dis) throws IOException
	{
		file = new File(dis.readUTF());
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(file);
			byte[] bytes = new byte[dis.readInt()];
			dis.read(bytes, 0, bytes.length);
			fos.write(bytes, 0, bytes.length);
			fos.flush();
			fos.close();
		}
		finally
		{
			if (fos != null)
			{
				fos.flush();
				fos.close();
			}
		}
	}

	public void write(DataOutputStream dos) throws IOException
	{
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			dos.writeUTF(file.getName());
			dos.writeInt((int) file.length());
			byte[] bytes = new byte[(int) file.length()];
			fis.read(bytes, 0, bytes.length);
			dos.write(bytes, 0, bytes.length);
			fis.close();
		}
		finally
		{
			if (fis != null)
				fis.close();
		}
	}

	private int setSize()
	{
		this.size += this.file.getName().length(); // name of file
		size += 4; // size of file
		size += (int) this.file.length(); // file
		return size;
	}
}