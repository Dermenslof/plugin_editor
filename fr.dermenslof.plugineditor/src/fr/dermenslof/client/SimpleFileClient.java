package fr.dermenslof.client;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import fr.dermenslof.plugineditor.ui.TreeObjectView;

public class SimpleFileClient
{
	public final static int		SOCKET_PORT	= 25665;
	public final static String	SERVER		= "127.0.0.1";

	public static boolean sendFile(File f)
	{
		try
		{
			Packet packet = new PacketFile(f);
			return send(packet);
		}
		catch (IOException e)
		{
			TreeObjectView.showMessage(e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	public static boolean sendLoad(String name, String jarName)
	{
		try
		{
			Packet packet = new PacketLoad(name, jarName);
			return send(packet);
		}
		catch (IOException e)
		{
			TreeObjectView.showMessage(e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	public static boolean send(Packet packet) throws IOException
	{
		DataOutputStream dos = null;
		Socket sock = null;
		try
		{
			sock = new Socket(SERVER, SOCKET_PORT);
			System.out.println("Connecting...");
			dos = new DataOutputStream(sock.getOutputStream());
			packet.handle(dos);
			if (packet instanceof PacketLoad)
				((PacketLoad) packet).write(dos);
			else if (packet instanceof PacketFile)
				((PacketFile) packet).write(dos);
		}
		finally
		{
			if (dos != null)
			{
				dos.flush();
				dos.close();
			}
			if (sock != null)
				sock.close();
		}
		return true;
	}
}