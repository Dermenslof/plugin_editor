package fr.dermenslof.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketLoad extends Packet
{
	public String	name;
	public String	jarName;

	public PacketLoad()
	{
		super();
	}

	public PacketLoad(String name, String jarName)
	{
		super();
		this.name = name;
		this.jarName = jarName;
		setSize();
	}

	public void read(DataInputStream dis) throws IOException
	{
		this.name = dis.readUTF();
		this.jarName = dis.readUTF();
	}

	public void write(DataOutputStream dos) throws IOException
	{
		dos.writeUTF(this.name);
		dos.writeUTF(this.jarName);
	}

	private void setSize()
	{
		size += this.name.length() + this.jarName.length();
	}
}