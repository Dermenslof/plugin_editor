package fr.dermenslof.model;

import java.util.ArrayList;
import java.util.List;

public class RepositoryObject extends Model
{
	protected static List<RepositoryObject>	newFileObjects	= new ArrayList<RepositoryObject>();
	protected static int				cursor			= 0;
	protected String url;
	protected String description;
	
	public RepositoryObject(String title, String url, String description)
	{
		super(title);
		this.url = url == null ? "" : url;
		this.description = description == null ? "" : description;
	}

	public static RepositoryObject newFileObject()
	{
		RepositoryObject newRepositoryObject = (RepositoryObject)newFileObjects.get(cursor);
		cursor = ((cursor + 1) % newFileObjects.size());
		return newRepositoryObject;
	}
	
	public String getUrl()
	{
		return this.url;
	}
	
	public String getDescription()
	{
		return this.description;
	}

	/*
	 * @see Model#accept(ModelVisitorI, Object)
	 */
	public void accept(IModelVisitor visitor, Object passAlongArgument)
	{
		visitor.visitRepositoryObject(this, passAlongArgument);
	}
}
