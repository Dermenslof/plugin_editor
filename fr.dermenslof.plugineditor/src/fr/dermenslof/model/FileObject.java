package fr.dermenslof.model;

import java.util.ArrayList;
import java.util.List;

public class FileObject extends Model
{
	protected static List<FileObject>	newFileObjects	= new ArrayList<FileObject>();
	protected static int				cursor			= 0;
	
	public FileObject(String title, String path, long size)
	{
		super(title, path, size);
	}

	public static FileObject newFileObject()
	{
		FileObject newFileObject = (FileObject)newFileObjects.get(cursor);
		cursor = ((cursor + 1) % newFileObjects.size());
		return newFileObject;
	}

	public void accept(IModelVisitor visitor, Object passAlongArgument)
	{
		visitor.visitFileObject(this, passAlongArgument);
	}
}