package fr.dermenslof.model;

public interface IModelVisitor
{
	public void visitMovingObject(TreeObject object, Object passAlongArgument);

	public void visitFileObject(FileObject fileObject, Object passAlongArgument);
	
	public void visitRepositoryObject(RepositoryObject repositoryObject, Object passAlongArgument);
}
