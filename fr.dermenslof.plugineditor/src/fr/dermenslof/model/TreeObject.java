package fr.dermenslof.model;

import java.util.ArrayList;
import java.util.List;

public class TreeObject extends Model
{
	protected List<TreeObject>			objects;
	protected List<FileObject>			fileObjects;
	protected List<RepositoryObject>	repositoryObjects;

	private static IModelVisitor		adder	= new Adder();
	private static IModelVisitor		remover	= new Remover();

	public TreeObject(String name, String path, long size)
	{
		super(name, path, size);
		objects = new ArrayList<TreeObject>();
		fileObjects = new ArrayList<FileObject>();
		repositoryObjects = new ArrayList<RepositoryObject>();
	}

	public TreeObject(String name, String path)
	{
		this(name, path, 0);
	}
	
	public TreeObject(String name)
	{
		this(name,null);
	}
	
	public String getPath()
	{
		return this.path;
	}
	
	public TreeObject()
	{
		this(null);
	}

	private static class Adder implements IModelVisitor
	{
		public void visitFileObject(FileObject fileObject, Object argument)
		{
			((TreeObject) argument).addFileObject(fileObject);
		}

		public void visitMovingObject(TreeObject object, Object argument)
		{
			((TreeObject) argument).addObject(object);
		}

		public void visitRepositoryObject(RepositoryObject repositoryObject, Object argument)
		{
			((TreeObject) argument).addRepositoryObject(repositoryObject);
		}

	}

	private static class Remover implements IModelVisitor
	{
		public void visitFileObject(FileObject fileObject, Object argument)
		{
			((TreeObject) argument).removeFileObject(fileObject);
		}

		public void visitMovingObject(TreeObject object, Object argument)
		{
			((TreeObject) argument).removeObject(object);
			object.addListener(NullDeltaListener.getSoleInstance());
		}

		@Override
		public void visitRepositoryObject(RepositoryObject repositoryObject, Object argument)
		{
			((TreeObject) argument).removeRepositoryObject(repositoryObject);
		}

	}

	public List<TreeObject> getObjects()
	{
		return objects;
	}

	protected void addObject(TreeObject object)
	{
		objects.add(object);
		object.parent = this;
		fireAdd(object);
	}

	protected void addFileObject(FileObject fileObject)
	{
		fileObjects.add(fileObject);
		fileObject.parent = this;
		fireAdd(fileObject);
	}

	public List<FileObject> getFileObjects()
	{
		return fileObjects;
	}

	protected void addRepositoryObject(RepositoryObject repositoryObject)
	{
		repositoryObjects.add(repositoryObject);
		repositoryObject.parent = this;
		fireAdd(repositoryObject);
	}

	public List<RepositoryObject> getRepositoryObjects()
	{
		return repositoryObjects;
	}

	public void remove(Model toRemove)
	{
		toRemove.accept(remover, this);
	}

	protected void removeFileObject(FileObject fileObject)
	{
		fileObjects.remove(fileObject);
		fileObject.addListener(NullDeltaListener.getSoleInstance());
		fireRemove(fileObject);
	}

	protected void removeRepositoryObject(RepositoryObject repositoryObject)
	{
		repositoryObjects.remove(repositoryObject);
		repositoryObject.addListener(NullDeltaListener.getSoleInstance());
		fireRemove(repositoryObject);
	}

	protected void removeObject(TreeObject object)
	{
		objects.remove(object);
		object.addListener(NullDeltaListener.getSoleInstance());
		fireRemove(object);
	}

	public void add(Model toAdd)
	{
		toAdd.accept(adder, this);
	}

	public int size()
	{
		return getFileObjects().size() + getObjects().size() + getRepositoryObjects().size();
	}

	public void accept(IModelVisitor visitor, Object passAlongArgument)
	{
		visitor.visitMovingObject(this, passAlongArgument);
	}
}