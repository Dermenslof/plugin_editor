package fr.dermenslof.model;

public abstract class Model
{
	protected TreeObject		parent;
	protected String			name;
	protected String			path;
	protected long				size;
	protected String			authorGivenName, authorSirName;
	protected IDeltaListener	listener	= NullDeltaListener.getSoleInstance();
	protected String			notice = null;

	protected void fireAdd(Object added)
	{
		listener.add(new DeltaEvent(added));
	}

	protected void fireRemove(Object removed)
	{
		listener.remove(new DeltaEvent(removed));
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public TreeObject getParent()
	{
		return parent;
	}
	
	public String getPath()
	{
		return this.path;
	}
	
	public boolean hasPath()
	{
		return this.path != null && !this.path.isEmpty();
	}
	
	public long getSize()
	{
		return this.size;
	}

	public boolean hasNotice()
	{
		return notice != null;
	}

	public void setNotice(String notice)
	{
		this.notice = notice;
	}

	public String getNotice()
	{
		return this.notice;
	}

	/*
	 * The receiver should visit the toVisit object and pass along the argument.
	 */
	public abstract void accept(IModelVisitor visitor, Object passAlongArgument);

	public String getName()
	{
		return name;
	}

	public void addListener(IDeltaListener listener)
	{
		this.listener = listener;
	}

	public Model()
	{

	}
	
	public Model(String title)
	{
		this(title, null, 0);
	}
	
	public Model(String name, String path, long size)
	{
		this.name = name;
		this.path = path;
		this.size = size;
	}

	public void removeListener(IDeltaListener listener)
	{
		if (this.listener.equals(listener))
		{
			this.listener = NullDeltaListener.getSoleInstance();
		}
	}

	public String authorGivenName()
	{
		return authorGivenName;
	}

	public String authorSirName()
	{
		return authorSirName;
	}

	public String getTitle()
	{
		return name;
	}
}