package fr.dermenslof.plugineditor.ui;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import fr.dermenslof.model.TreeObject;

public class TreeItemFilter extends ViewerFilter
{
	/*
	 * @see ViewerFilter#select(Viewer, Object, Object)
	 */
	public boolean select(Viewer viewer, Object parentElement, Object element)
	{
		return parentElement instanceof TreeObject && ((TreeObject) parentElement).size() >= 3;
	}
}
