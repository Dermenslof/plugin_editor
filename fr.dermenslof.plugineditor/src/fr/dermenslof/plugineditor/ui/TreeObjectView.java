package fr.dermenslof.plugineditor.ui;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import fr.dermenslof.client.SimpleFileClient;
import fr.dermenslof.git.ExporterJar;
import fr.dermenslof.git.GitUtils;
import fr.dermenslof.git.UsernamePasswordDialog;
import fr.dermenslof.model.FileObject;
import fr.dermenslof.model.Model;
import fr.dermenslof.model.RepositoryObject;
import fr.dermenslof.model.TreeObject;
import fr.dermenslof.project.ProjectUtil;

public class TreeObjectView extends ViewPart
{
	public static TreeViewer			treeViewer;
	protected Text						text;
	protected TreeObjectLabelProvider	labelProvider;

	protected Action					sendJarAction,buildAction;
	protected Action					connectAction, pullAction, pushAction, cloneAction;
	protected Action					atLeatThreeItems;
	protected Action					fileObjectsObjectsGamesAction, noArticleAction;
	protected Action					addFileObjectAction, removeAction;
	protected ViewerFilter				atLeastThreeFilter;
	protected ViewerSorter				fileObjectsObjectsGamesSorter, noArticleSorter;

	protected TreeObject				root, builds, repos;
	public static boolean				redraw;

	public TreeObjectView()
	{

	}

	public void createPartControl(Composite parent)
	{
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 2;
		layout.marginWidth = 0;
		layout.marginHeight = 2;
		parent.setLayout(layout);

		text = new Text(parent, SWT.READ_ONLY | SWT.SINGLE | SWT.BORDER);
		GridData layoutData = new GridData();
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.horizontalAlignment = GridData.FILL;
		text.setLayoutData(layoutData);

		treeViewer = new TreeViewer(parent);
		treeViewer.setContentProvider(new TreeObjectContentProvider());
		labelProvider = new TreeObjectLabelProvider();
		treeViewer.setLabelProvider(labelProvider);

		treeViewer.setUseHashlookup(true);

		layoutData = new GridData();
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.grabExcessVerticalSpace = true;
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.verticalAlignment = GridData.FILL;
		treeViewer.getControl().setLayoutData(layoutData);

		startRefreshTask();
		createFiltersAndSorters();
		createActions();
		createMenus();
		createToolbar();
		hookListeners();
		hookContextMenu();

		// treeViewer.setInput(getInitalInput());
		// treeViewer.expandAll();
	}

	private UIJob	refreshUIJob;

	private void startRefreshTask()
	{
		if (refreshUIJob != null)
			refreshUIJob.cancel();
		refreshUIJob = new UIJob("Acceleo") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor)
			{
				startRefreshTask();
				final Status status = new Status(IStatus.OK, Activator.PLUGIN_ID, "OK");
				if (!PluginEditor.isConnected)
					return status;
				if (treeViewer == null || treeViewer.getTree() == null || treeViewer.getTree().isDisposed())
					return status;
				if (TreeObjectView.redraw)
				{
					treeViewer.setInput(getInitalInput());
					TreeObjectView.redraw = false;
					treeViewer.expandToLevel(3);
				}
				treeViewer.refresh();
				return status;
			}
		};
		refreshUIJob.setPriority(Job.DECORATE);
		refreshUIJob.setSystem(true);
		refreshUIJob.setProgressGroup(new NullProgressMonitor(), 1);
		final int schedule = 2000;
		refreshUIJob.schedule(schedule);
		refreshUIJob = null;
	}

	protected void createFiltersAndSorters()
	{
		atLeastThreeFilter = new TreeItemFilter();
		fileObjectsObjectsGamesSorter = new FileObjectBoardSorter();
		noArticleSorter = new NoArticleSorter();
	}

	protected void hookListeners()
	{

		text.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e)
			{
				e.gc.setBackground(text.getBackground());
				e.gc.fillRectangle(0, 0, text.getBounds().width, text.getBounds().height);
				e.gc.setForeground(text.getForeground());
				e.gc.drawString(text.getText(), 5, 1);
			}
		});

		treeViewer.getTree().addMouseMoveListener(new MouseMoveListener() {
			Tree	tree	= treeViewer.getTree();

			@Override
			public void mouseMove(MouseEvent e)
			{
				String s = "";
				TreeItem item = tree.getItem(new Point(e.x, e.y));
				// text.
				text.setBackground(new Color(text.getDisplay(), 128, 128, 128));
				text.setForeground(new Color(text.getDisplay(), 0, 0, 0));
				if (item != null)
				{
					Model o = (Model) treeViewer.getCell(new Point(e.x, e.y)).getElement();
					if (o.hasNotice())
					{
						text.setForeground(new Color(text.getDisplay(), 120, 0, 0));
						s = o.getNotice();
					}
					else
						s = o.getTitle();
				}
				text.setText(s);
			}
		});
	}

	protected void hookContextMenu()
	{
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager)
			{
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, treeViewer);
	}

	protected void createActions()
	{
		sendJarAction = new Action("Send") {
			public void run()
			{
				sendJar();
			}
		};
		
		buildAction = new Action("Build") {
			public void run()
			{
				buildProject();
			}
		};

		pullAction = new Action("Pull") {
			public void run()
			{
				pullProject();
			}
		};

		pushAction = new Action("Push All") {
			public void run()
			{
				pushProject();
			}
		};

		cloneAction = new Action("Clone") {
			public void run()
			{
				cloneProject();
			}
		};

		final ISharedImages images = PlatformUI.getWorkbench().getSharedImages();
		connectAction = new Action() {
			public void run()
			{
				if (!PluginEditor.isConnected)
				{
					UsernamePasswordDialog dlg = new UsernamePasswordDialog(Display.getCurrent().getActiveShell());
					if (dlg.open() != Window.OK)
						return;
					String msg = GitUtils.connect();
					if (msg == null)
					{
						treeViewer.setInput(getInitalInput());
						treeViewer.expandToLevel(3);
						this.setImageDescriptor(images.getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED));
						PluginEditor.isConnected = true;
					}
					else
						showMessage(msg);
				}
				else
				{
					root = null;
					treeViewer.setInput(root);
					treeViewer.refresh();
					this.setImageDescriptor(images.getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED_DISABLED));
					PluginEditor.isConnected = false;
				}
			}
		};
		connectAction.setImageDescriptor(images.getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED_DISABLED));

		atLeatThreeItems = new Action("Objects With At Least Three Items") {
			public void run()
			{
				updateFilter(atLeatThreeItems);
			}
		};
		atLeatThreeItems.setChecked(false);

		fileObjectsObjectsGamesAction = new Action("Folder, Objects") {
			public void run()
			{
				updateSorter(fileObjectsObjectsGamesAction);
			}
		};
		fileObjectsObjectsGamesAction.setChecked(false);

		noArticleAction = new Action("Ignoring Articles") {
			public void run()
			{
				updateSorter(noArticleAction);
			}
		};
		noArticleAction.setChecked(false);

		addFileObjectAction = new Action("Create New Repository") {
			public void run()
			{
				addNewFileObject();
			}
		};
		addFileObjectAction.setToolTipText("Create new repository");
		addFileObjectAction.setImageDescriptor(PluginEditor.getImageDescriptor("newFile.gif"));

		removeAction = new Action("Remove") {
			public void run()
			{
				removeSelected();
			}
		};
		removeAction.setToolTipText("Delete Repository");
		removeAction.setImageDescriptor(PluginEditor.getImageDescriptor("remove.gif"));
	}

	protected void addNewFileObject()
	{
		TreeObject receivingObject;
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model selectedDomainObject = (Model) selection.getFirstElement();
			if (!(selectedDomainObject instanceof TreeObject))
				receivingObject = selectedDomainObject.getParent();
			else
				receivingObject = (TreeObject) selectedDomainObject;
		}
		String name = "plugin_";
		InputDialog dlg = new InputDialog(Display.getCurrent().getActiveShell(), "", "Repository name: ", "", null);
		if (dlg.open() == Window.OK && !dlg.getValue().trim().isEmpty())
			name += dlg.getValue().trim();
		else
			return;
		if (ProjectUtil.createProject(name) == null
				&& GitUtils.getLocalGit(new File(GitUtils.getWorkspacePath(), name)) == null)
			return;
		Repository repo = GitUtils.createRemoteRepo(name);
		if (repo != null)
		{
			if (!new File(GitUtils.getWorkspacePath(), name + "/.git").exists())
				GitUtils.init(name, repo.getCloneUrl().replace("[.]git", ""));
			receivingObject.add(new RepositoryObject(repo.getName(), repo.getCloneUrl(), repo.getDescription()));
			treeViewer.expandAll();
			treeViewer.refresh();
		}
	}

	protected void pullProject()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model obj = (Model) selection.getFirstElement();
			if (obj instanceof RepositoryObject)
			{
				GitUtils.pull(obj.getName());
				treeViewer.refresh();
			}
		}
	}

	protected void pushProject()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model obj = (Model) selection.getFirstElement();
			if (obj instanceof RepositoryObject)
			{
				GitUtils.pushAll(obj.getName());
				treeViewer.refresh();
			}
		}
	}
	
	protected void buildProject()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model obj = (Model) selection.getFirstElement();
			if (obj instanceof RepositoryObject)
			{
				new Thread(new ExporterJar(obj.getName())).start();
				TreeObjectView.redraw = true;
			}
		}
	}

	protected void sendJar()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model obj = (Model) selection.getFirstElement();
			if (obj instanceof FileObject)
			{
				if (SimpleFileClient.sendFile(new File(((FileObject) obj).getPath())))
				{
					if (SimpleFileClient.sendLoad(obj.getParent().getName(), obj.getName()))
						showMessage("File send !");
				}					
			}
		}
	}

	protected void cloneProject()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		else
		{
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			Model obj = (Model) selection.getFirstElement();
			if (obj instanceof RepositoryObject)
			{
				GitUtils.clone(((RepositoryObject) obj).getUrl(), obj.getName());
				treeViewer.refresh();
			}
		}
	}
	
	protected void deleteFile(File file)
	{
		if (file.isDirectory())
		{
			File[] files = file.listFiles();
			for (File f : files)
				deleteFile(f);
		}
		file.delete();
	}

	protected void removeSelected()
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
		if (!showQuestion("Are you sure ?"))
			return;
		treeViewer.getTree().setRedraw(false);
		for (Iterator<?> iterator = selection.iterator(); iterator.hasNext();)
		{
			Model model = (Model) iterator.next();
			if (model instanceof TreeObject || model instanceof FileObject)
			{
				File f = new File(model.getPath());
				if (f.exists())
				{
					deleteFile(f);
					while (f.getParentFile().isDirectory() && f.getParentFile().listFiles().length == 0)
					{
						f = f.getParentFile();
						f.delete();
					}
				}
				TreeObject parent = model.getParent();
				parent.remove(model);
				while (!parent.getName().equals("builds") && parent.getObjects().size() < 1)
				{
					model = parent;
					parent = parent.getParent();
					parent.remove(model);
				}
			}
		}
		treeViewer.getTree().setRedraw(true);
	}

	protected void createMenus()
	{
		IMenuManager rootMenuManager = getViewSite().getActionBars().getMenuManager();
		rootMenuManager.setRemoveAllWhenShown(true);
		rootMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr)
			{
				fillMenu(mgr);
			}
		});
		fillMenu(rootMenuManager);
	}

	protected void fillContextMenu(IMenuManager manager)
	{
		if (treeViewer.getSelection().isEmpty())
			return;
		Model m = (Model) ((IStructuredSelection) treeViewer.getSelection()).getFirstElement();
		boolean isRepoDirectory = m.getName().equals("Repository");
		boolean isRepo = m instanceof RepositoryObject;
		boolean isFile = m instanceof FileObject;
		if (isFile && (!isRepoDirectory && !isRepo))
		{
			manager.add(sendJarAction);
			manager.add(removeAction);
		}
		if (!isRepoDirectory && !isRepo)
			return;
		// manager.add(addFileObjectAction);
		if (isRepoDirectory)
		{
			manager.add(addFileObjectAction);
			return;
		}
		// manager.add(removeAction);
		if (!isRepo)
			return;
		if (!m.hasNotice() || (m.hasNotice() && !m.getNotice().contains("locally")))
		{
			manager.add(pullAction);
			if (m.hasNotice() && m.getNotice().contains("sync"))
				manager.add(pushAction);
		}
		if (m.hasNotice() && m.getNotice().contains("locally"))
			manager.add(cloneAction);
		if (!m.hasNotice())
			manager.add(buildAction);
	}

	protected void fillMenu(IMenuManager rootMenuManager)
	{
		IMenuManager filterSubmenu = new MenuManager("Filters");
		rootMenuManager.add(filterSubmenu);
		filterSubmenu.add(atLeatThreeItems);

		IMenuManager sortSubmenu = new MenuManager("Sort By");
		rootMenuManager.add(sortSubmenu);
		sortSubmenu.add(fileObjectsObjectsGamesAction);
		sortSubmenu.add(noArticleAction);
	}

	protected void updateSorter(Action action)
	{
		if (action == fileObjectsObjectsGamesAction)
		{
			noArticleAction.setChecked(!fileObjectsObjectsGamesAction.isChecked());
			if (action.isChecked())
				treeViewer.setSorter(fileObjectsObjectsGamesSorter);
			else
				treeViewer.setSorter(null);
		}
		else if (action == noArticleAction)
		{
			fileObjectsObjectsGamesAction.setChecked(!noArticleAction.isChecked());
			if (action.isChecked())
				treeViewer.setSorter(noArticleSorter);
			else
				treeViewer.setSorter(null);
		}
	}

	protected void updateFilter(Action action)
	{
		if (action == atLeatThreeItems)
		{
			if (action.isChecked())
				treeViewer.addFilter(atLeastThreeFilter);
			else
				treeViewer.removeFilter(atLeastThreeFilter);
		}
	}

	protected void createToolbar()
	{
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
		toolbarManager.add(connectAction);
		toolbarManager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		// toolbarManager.add(addFileObjectAction);
		// toolbarManager.add(removeAction);
	}

	public TreeObject getInitalInput()
	{
		root = new TreeObject();
		repos = new TreeObject("Repository", null);
		root.add(repos);
		List<Repository> list = GitUtils.explore();
		for (Repository repo : list)
		{
			if (repo.getName().startsWith("plugin_"))
			{
				RepositoryObject o = new RepositoryObject(repo.getName(), repo.getCloneUrl(), repo.getDescription());
				repos.add(o);
			}
		}
		builds = new TreeObject("builds");
		root.add(builds);
		File base = new File(GitUtils.getWorkspacePath(), "builds_plugin");
		if (base.exists())
		{
			for (File f : base.listFiles())
				createTreeFile(f, builds);
		}
		return root;
	}

	protected void createTreeFile(File folder, TreeObject parent)
	{
		TreeObject base = new TreeObject(folder.getName(), folder.getParent());
		for (File f : folder.listFiles())
		{
			if (f.isDirectory())
			{
				createTreeFile(f, base);
				treeViewer.refresh(base);
			}
			else
			{
				FileObject o = new FileObject(f.getName(), f.getAbsolutePath(), f.length());
				base.add(o);
				treeViewer.refresh(base);
			}
		}
		parent.add(base);
		treeViewer.refresh(parent);
	}

	public void setFocus()
	{

	}

	public static void showMessage(String message)
	{
		MessageDialog.openInformation(treeViewer.getControl().getShell(), "PluginEditor", message);
	}

	public static boolean showQuestion(String message)
	{
		return MessageDialog.openConfirm(treeViewer.getControl().getShell(), "PluginEditor", message);
	}
}