package fr.dermenslof.plugineditor.ui;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import fr.dermenslof.git.GitUtils;
import fr.dermenslof.model.FileObject;
import fr.dermenslof.model.Model;
import fr.dermenslof.model.RepositoryObject;
import fr.dermenslof.model.TreeObject;

public class TreeObjectLabelProvider extends LabelProvider
{
	private Map<ImageDescriptor, Image>	imageCache	= new HashMap<ImageDescriptor, Image>(11);

	public Image getImage(Object element)
	{
		ImageDescriptor descriptor = null;
		if (element instanceof TreeObject)
		{
			if (((TreeObject) element).hasPath())
				descriptor = PluginEditor.getImageDescriptor("folder.gif");
			else
				descriptor = PluginEditor.getImageDescriptor("item.gif");
		}
		else
		{
			if (element instanceof FileObject)
				descriptor = PluginEditor.getImageDescriptor("jar.gif");
			else if (element instanceof RepositoryObject)
			{
				String name = ((RepositoryObject)element).getName();
				if (GitUtils.getLocalGit(new File(GitUtils.getWorkspacePath(), name)) == null)
				{
					((Model)element).setNotice("can't find repository locally");
					descriptor = PluginEditor.getImageDescriptor("git-red.gif");
				}
				else if (GitUtils.status(name))
				{
					((Model)element).setNotice("repository not sync");
					descriptor = PluginEditor.getImageDescriptor("git-gold.gif");
				}
				else
				{
					((Model)element).setNotice(null);
					descriptor = PluginEditor.getImageDescriptor("git-green.gif");
				}
			}
			else
				throw unknownElement(element);
		}
		// obtain the cached image corresponding to the descriptor
		Image image = (Image) imageCache.get(descriptor);
		if (image == null)
		{
			image = descriptor.createImage();
			imageCache.put(descriptor, image);
		}
		return image;
	}

	public String getText(Object element)
	{
		if (element instanceof Model)
		{
			Model m = (Model)element;
			if (m instanceof TreeObject)
			{
				if (((TreeObject) element).getName() == null)
					return "Object";
				return ((TreeObject) element).getName();
			}
			else if (m instanceof FileObject)
				return m.getName() + (m.hasPath() ? " - "  + getSize(m.getSize()) : "");
			else if (element instanceof RepositoryObject)
				return ((RepositoryObject) element).getTitle();
		}
		throw unknownElement(element);
	}
	
	private String getSize(long size)
	{
		long tmp = size;
		int i = 0;
		while (tmp / 1000 > 0)
		{
			++i;
			tmp /= 1000;
		}
		switch (i)
		{
			case 1:
				return "" + tmp + "." + (size - tmp * 1000) + " Ko";
			case 2:
				return "" + tmp + "." + (size - tmp * 1000000) + " Mo";
		}
		return "" + size + " octets";
	}

	public void dispose()
	{
		for (Iterator<Image> i = imageCache.values().iterator(); i.hasNext();)
			((Image) i.next()).dispose();
		imageCache.clear();
	}

	protected RuntimeException unknownElement(Object element)
	{
		return new RuntimeException("Unknown type of element in tree of type " + element.getClass().getName());
	}
}