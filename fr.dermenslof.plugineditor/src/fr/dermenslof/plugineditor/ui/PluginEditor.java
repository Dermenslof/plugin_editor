package fr.dermenslof.plugineditor.ui;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * The main plugin class to be used in the desktop.
 */
public class PluginEditor extends AbstractUIPlugin
{
	// The shared instance.
	private static PluginEditor	plugin;
	// Resource bundle.
	private ResourceBundle		resourceBundle;
	
	// test
	public static boolean isConnected;

	public PluginEditor()
	{
		plugin = this;
		try
		{
			resourceBundle = ResourceBundle.getBundle("fr.dermenslof.plugineditor.PluginEditorResources");
		}
		catch (MissingResourceException x)
		{
			resourceBundle = null;
		}
	}

	/**
	 * Returns the shared instance.
	 */
	public static PluginEditor getDefault()
	{
		return plugin;
	}

	/**
	 * Returns the workspace instance.
	 */
	public static IWorkspace getWorkspace()
	{
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Returns the string from the plugin's resource bundle, or 'key' if not
	 * found.
	 */
	public static String getResourceString(String key)
	{
		ResourceBundle bundle = PluginEditor.getDefault().getResourceBundle();
		try
		{
			return bundle.getString(key);
		}
		catch (MissingResourceException e)
		{
			return key;
		}
	}

	public static ImageDescriptor getImageDescriptor(String name)
	{
		return imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/" + name);
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle()
	{
		return resourceBundle;
	}
}