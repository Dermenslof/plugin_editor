package fr.dermenslof.plugineditor.ui;

import org.eclipse.jface.viewers.ViewerSorter;

import fr.dermenslof.model.FileObject;
import fr.dermenslof.model.TreeObject;

public class FileObjectBoardSorter extends ViewerSorter
{
	/*
	 * @see ViewerSorter#category(Object)
	 */
	/**
	 * Orders the items in such a way that fileObjects appear before moving boxes,
	 * which appear before board games.
	 */
	public int category(Object element)
	{
		if (element instanceof FileObject)
			return 1;
		if (element instanceof TreeObject)
			return 2;
		return 3;
	}

}