package fr.dermenslof.project;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.PreferenceConstants;

import fr.dermenslof.git.GitUtils;
import fr.dermenslof.plugineditor.ui.Privacy;

public class ProjectUtil
{
	public static String[]	plugin_src	= { "package %plugin_package%;", "import org.bukkit.plugin.java.JavaPlugin;",
			"", "public class %plugin_class% extends JavaPlugin", "{", "\t@Override", "\tpublic void onEnable()", "\t{",
			"\t\tgetLogger().info(\"ok\");", "\t}", "}" };
	
	public static String[]	plugin_yml	= { "name: %plugin_name%", "author: %author%", "version: 0.0.1", "main: %main_class%"};

	public static IProject createProject(String name)
	{
		IProgressMonitor pm = new NullProgressMonitor();
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(name);
		try
		{
			project.create(pm);
			project.open(pm);
			IProjectDescription desc = project.getDescription();
			desc.setNatureIds(new String[]
				{ JavaCore.NATURE_ID });
			project.setDescription(desc, pm);
			IJavaProject javaProj = JavaCore.create(project);
			IFolder binDir = project.getFolder("bin");
			IPath binPath = binDir.getFullPath();
			javaProj.setOutputLocation(binPath, pm);
			IFolder sourceFolder = project.getFolder("src");
			sourceFolder.create(false, true, pm);
			IPackageFragmentRoot packageRoot = javaProj.getPackageFragmentRoot(sourceFolder);
			List<IClasspathEntry> cpes = new ArrayList<IClasspathEntry>();
			cpes.add(JavaCore.newSourceEntry(packageRoot.getPath()));
			cpes.addAll(Arrays.asList(PreferenceConstants.getDefaultJRELibrary()));
			cpes.add(JavaCore.newLibraryEntry(
					new Path("/home/dermenslof/WarnFight/libs/spigot-1.8.3-R0.1-SNAPSHOT.jar"), null, null));
			javaProj.setRawClasspath(cpes.toArray(new IClasspathEntry[cpes.size()]), pm);
			addMasterClass(sourceFolder, name);
			addPluginYml(sourceFolder, name);
			project.refreshLocal(IProject.DEPTH_INFINITE, pm);
			
			return project;
		}
		catch (CoreException e)
		{
			 e.printStackTrace();
		}
		return null;
	}

	private static String capitalize(String str)
	{
		str = str.toLowerCase();
		String[] split = str.split("_");
		StringBuilder sb = new StringBuilder();
		for (String s : split)
		{
			String capital = s.substring(0, 1);
			String tmp = s.substring(1, s.length());
			char c = capital.charAt(0);
			if (c >= 'a' && c <= 'z')
				c -= 32;
			sb.append(c).append(tmp);
		}
		return sb.toString();
	}

	private static void addMasterClass(IFolder folder, String name)
	{
		String className = capitalize(name);
		File pack = new File(GitUtils.getWorkspacePath() + "/" + folder.toString().substring(2), name.replaceAll("_",
				""));
		pack.mkdirs();
		File f = new File(pack, className + ".java");
		FileOutputStream fos;
		try
		{
			fos = new FileOutputStream(f);

			for (String s : plugin_src)
			{
				s = s.replace("%plugin_package%", pack.getName());
				s = s.replace("%plugin_class%", className);
				for (int i = 0; i < s.length(); ++i)
					fos.write(s.charAt(i));
				fos.write('\n');
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	private static void addPluginYml(IFolder folder, String name)
	{
		File f = new File(GitUtils.getWorkspacePath() + "/"  + folder.toString().substring(2), "plugin.yml");
		FileOutputStream fos;
		try
		{
			fos = new FileOutputStream(f);

			for (String s : plugin_yml)
			{
				s = s.replace("%plugin_name%", name);
				s = s.replace("%author%", capitalize(Privacy.user.toLowerCase()));
				s = s.replace("%main_class%", name.replaceAll("_", "") + "." + capitalize(name));
				for (int i = 0; i < s.length(); ++i)
					fos.write(s.charAt(i));
				fos.write('\n');
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}